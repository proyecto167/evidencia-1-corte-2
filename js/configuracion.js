// configuracion firebase

    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
    import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
    import{ getDatabase, onValue, ref ,set,child,get,update,remove} from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
    import { getStorage, ref as refS, uploadBytes, getDownloadURL  } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Login
    import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";


    // Your web app's Firebase configuration
    const firebaseConfig = {
    apiKey: "AIzaSyBfvzuXNf0ds981EpPYO56Lrlatc7uvwHo",
    authDomain: "dbweb1-2794e.firebaseapp.com",
    projectId: "dbweb1-2794e",
    storageBucket: "dbweb1-2794e.appspot.com",
    messagingSenderId: "557433984948",
    appId: "1:557433984948:web:31a1c696b2759df276c4e1"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const db=getDatabase();

// declaracion de Objetos
var id = "";
var nombre = "";
var descripcion = "";
var precio = "";
var estado = "";
var url = "";

// Botones
var btnAgregar = document.getElementById('btnAgregar');
var btnMostrar = document.getElementById('btnMostrar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
var btnTodos = document.getElementById('btnTodos');
var lista = document.getElementById('lista');
var btnLimpiar = document.getElementById('btnLimpiar');
var archivo=document.getElementById('archivo');

var btnLogin=document.getElementById('btnLogin');
var btnLim=document.getElementById('btnLim');

var articulos = document.getElementById('row');

if(window.location.href == "http://127.0.0.1:5500/html/productos.html"){
  window.onload = mostrarProductos();
}

/* function mostrarAlumnos(){
    const db = getDatabase();
    const dbRef = ref(db, 'alumnos');
    onValue(dbRef, (snapshot) => {
     lista.innerHTML=""
     snapshot.forEach((childSnapshot) => {
     const childKey = childSnapshot.key;
     const childData = childSnapshot.val();
    
     lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey +" "+
    childData.nombre  +" "+ childData.carrera  +" "+childData.genero +"<br> </div>";
     console.log(childKey + ":");
     console.log(childData.nombre)
     // ...
     });
    }, {
     onlyOnce: true
    });
} */
var txtEmail="";
var txtPassword="";

  // Initialize Firebase Authentication and get a reference to the service
  const auth = getAuth(app);
  

    function leer(){
        txtEmail=document.getElementById('emailLogin').value;
        txtPassword=document.getElementById('passwordLogin').value;
    }

    function ComprobarAuth(){
        onAuthStateChanged(auth, (user) => {
            if (user) {
              alert("Usuario detectado");
              //document.getElementById('todo').style.display="block";
              // ...
            } else {
              // User is signed out
              // ...
              alert("No se ha detectado un usuario");
              window.location.href="index.html";
            }
        });
    }

    if(window.location.href == "http://127.0.0.1:5500/html/administrador.html"){
        window.onload = ComprobarAuth();
    }

    if(btnLogin){
        btnLogin.addEventListener('click', (e)=>{
          leer();
          signInWithEmailAndPassword(auth, txtEmail, txtPassword)
            .then((userCredential) => {
              // Signed in 
              const user = userCredential.user;
              alert("Has iniciado sesión.")
              window.location.href="administrador.html";
              // ...
            })
            .catch((error) => {
              alert("Usuario o contraseña incorrecta.")
              const errorCode = error.code;
              const errorMessage = error.message;
            });
        });
    }


  /*const loginEmailPassword = async () => {
    const loginEmail = txtEmail;
    const loginPassword = txtPassword;

    try{
        const userCredential = await signInWithEmailAndPassword(auth, loginEmail, loginPassword);
        console.log(userCredential.user);
    }
    catch(error){
        console.log(error);
        if(error.code == AuthErrorCodes.INVALID_PASSWORD){
            alert("Contraseña Incorrecta.");
        } else{
            alert("Datos incorrectos.");
        }
    }
  }*/

/*const monitorAuthState = async () => {
    onAuthStateChanged(auth, user => {
        if(user){
            alert("Bienvenido!.")
        }
    });
}*/

//monitorAuthState();

    /*.then((userCredential) => {
      // Signed in 
      const user = userCredential.user;
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
    });*/

function mostrarProductos(){
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
        if(lista){
            lista.innerHTML = "";
        } 
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if(lista){
                lista.innerHTML = lista.innerHTML + "<div class='registro'> " + "<img class='imgAdmin' src=' " + childData.url + "'>"+"<h2>Id: " + childKey + "</h2><h2>Nombre: " + childData.nombre + "</h2><p>Descripción:"  + childData.descripcion + "</p><p>URL imagen:"  + childData.url + "</p><p> Precio: $" + childData.precio+ "</p>"+"<p> Estado:" + childData.estado + "</p>";
            } else if(articulos){
                if(childData.estado==1){
                    articulos.innerHTML= articulos.innerHTML+"<div class='articulo'><picture><img class='imgprod' src='"+childData.url+"'></picture><br><p class='productonom'>"+childData.nombre +"</p> <br><p class='reseña'>"+childData.descripcion+"</p><br><p class='precio'>$"+childData.precio+"</p></div>";
                }
                
            }
            
        });
    },{
        onlyOnce: true
    });

}


function leerInputs(){
    id = document.getElementById('id').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    estado = document.getElementById('estado').value;
    url = document.getElementById('url').value;
}


/*async*/ function insertarDatos(){
    
    subirArchivo();

    setTimeout(leerInputs,5000);

    
        setTimeout(()=>{
            if(id!="" && nombre!="" && descripcion!="" && precio!="" && estado!=""){
                set(ref(db,'productos/' + id),{
                    nombre: nombre,
                    descripcion:descripcion,
                    precio,precio,
                    estado:estado,
                    url:url})
                    .then((response) => {
                    alert("Registro exitoso");
                    mostrarProductos();
                    //mostrarDatos();
                    //console.log("datos" + matricula + nombre + carrera+ genero)
                    })
                    .catch((error) => {
                    alert("Error en el registro.")
                });
            } else{
                alert("Asegurate de que registraste todos los campos.");
            }
        }, 5000);
    
    

    //var estado = document.getElementById('estado').value;
    

    //alert ("Se agregó.");

    /*const dbref=ref(db);
    get
        (child(dbref,'alumnos/'+matricula))
        .then((snapshot)=>{
            if(snapshot.exists()){
                nombre=snapshot.val().nombre;
                carrera=snapshot.val().carrera;
                genero=snapshot.val().genero;
                // Poner los datos en los inputs
                llenarInputs();
            }
            else{
                alert("No existe el registro solicitado");
            }
        }).catch((error)=>{
            alert("Surgió un error "+error);
        })*/
}

async function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
    
    if(id!=""){
        await get(child(dbref,'productos/' + id)).then((snapshot)=>{
            if (snapshot.exists()) {
                nombre = snapshot.val().nombre;
                descripcion = snapshot.val().descripcion;
                precio = snapshot.val().precio;
                estado = snapshot.val().estado;
                url = snapshot.val().url;
    
                escribirInputs();
                
            }else{
                alert("No existe el producto.");
            }
        }).catch((error)=>{
            alert("Se encontró un error: " + error);
        });
    } else{
        alert("No existe el producto.");
    }
        
    }
    


/*async*/ function actualizar(){

    subirArchivo();

    setTimeout(leerInputs,5000);
    const dbref = ref(db);
    
    setTimeout(()=>{
        if(id!="" && nombre!="" && descripcion!="" && precio!="" && estado!=""){
            get(child(dbref,'productos/' + id)).then((snapshot)=>{
                if (snapshot.exists()) {
                    update(ref(db,'productos/'+ id),{
                        nombre: nombre,
                        descripcion:descripcion,
                        precio,precio,
                        estado:estado,
                        url:url
                    }).then(()=>{
                        alert("se realizó la actualización.");
                        mostrarProductos();
                    })
                    .catch((error)=>{
                        alert("Se encontró un error. " + error );
                    });
                    
                }else{
                    alert("No existe el producto.");
                }
            }).catch((error)=>{
                alert("Se encontró un error: " + error);
            });
    } else{
        alert("Asegurate de que registraste todos los campos.");
    }
    }, 5000);


    /*setTimeout(()=>{
        update(ref(db,'productos/'+ id),{
            nombre: nombre,
            descripcion:descripcion,
            precio,precio,
            estado:estado,
            url:url
           }).then(()=>{
            alert("se realizó la actualización.");
            mostrarProductos();
           })
           .catch((error)=>{
            alert("Se encontró un error. " + error );
           });
        }, 5000);*/

    
}

function escribirInputs(){
    document.getElementById('id').value= id;
    document.getElementById('nombre').value=nombre;
    document.getElementById('descripcion').value=descripcion;
    document.getElementById('precio').value=precio;
    document.getElementById('estado').value=estado;
    document.getElementById('url').value=url;
}

async function borrar(){
    leerInputs();
    const dbref = ref(db);
    
    if(id!=""){
        await get(child(dbref,'productos/' + id)).then((snapshot)=>{
            if (snapshot.exists()) {
                update(ref(db,'productos/'+ id),{
                    estado:"0"
                   }).then(()=>{
                    alert("Se deshabilitó el registro.");
                    mostrarProductos();
                   })
                   .catch((error)=>{
                    alert("Se encontró un error. " + error );
                   });
                
            }else{
                alert("No existe el producto.");
            }
        }).catch((error)=>{
            alert("Se encontró un error: " + error);
        });
    } else{
        alert("No existe el producto.");
    }


    /*if(id==""){
        alert("No existe esa id.");
        return;
    } else if(id!=null){
        update(ref(db,'productos/'+ id),{
            estado:"0"
           }).then(()=>{
            alert("Se deshabilitó el registro.");
            mostrarProductos();
           })
           .catch((error)=>{
            alert("Se encontró un error. " + error );
           });

    }*/

        /*await remove(ref(db,'productos/'+ id)).then(()=>{
            alert("Se deshabilitó el registro.");
            mostrarProductos();
            })
            .catch((error)=>{
            alert("Se encontró un error. " + error );
            });*/
    
    
   }



function limpiar(){
    lista.innerHTML="";
    id="";
    nombre="";
    descripcion="";
    precio="";
    estado=0;
    url="";
    document.getElementById('MuestraImagen').src="";
    escribirInputs();
}

var file="";
var name="";

async function cargarImagen(){

    // archivo seleccionado
    file=event.target.files[0];
    name=event.target.files[0].name;

    //document.getElementById('imgNombre').value = name;

    /*await*/ /*uploadBytes(storageRef, file).then((snapshot) => {
        console.log('Se cargó el archivo.');
        url=descargarImagen(name);


    });
    console.log(url); */
}

async function subirArchivo(){
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);

    // 'file' comes from the Blob or File API
    await uploadBytes(storageRef, file).then((snapshot) => {
        alert("Se cargo el archivo");
    });

    descargarImagen();
}

async function descargarImagen(){
    //alert(nombre);
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/'+name);

    // Get the download URL
    await getDownloadURL(storageRef)
    .then((url) => {
    // Insert url into an <img> tag to "download"
    //console.log(url);
    document.getElementById('url').value = url;
    //document.getElementById('MuestraImagen').src=url;
    })
    .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
            case 'storage/object-not-found':
            // File doesn't exist
            alert("El archivo no existe. " + error );
            break;
            case 'storage/unauthorized':
            // User doesn't have permission to access the object
            alert("El usuario no tiene permiso para acceder a este archivo. " + error );
            break;
            case 'storage/canceled':
            // User canceled the upload
            alert("El usuario canceló la subida del archivo. " + error );
            break;

            // ...

            case 'storage/unknown':
            // Unknown error occurred, inspect the server response
            alert("Ocurrió un error desconocido. " + error );
            break;
        }
    });
}

// Eventos change
if(archivo){
    archivo.addEventListener('change',cargarImagen);
}


// Codificar evento click
if(btnAgregar){
    btnAgregar.addEventListener('click',insertarDatos);
}
if(btnMostrar){
    btnMostrar.addEventListener('click',mostrarDatos);
}
if(btnActualizar){
    btnActualizar.addEventListener('click',actualizar);
}
if(btnBorrar){
    btnBorrar.addEventListener('click',borrar);
}
if(btnTodos){
    btnTodos.addEventListener('click', mostrarProductos);
}
if(btnLimpiar){
    btnLimpiar.addEventListener('click', limpiar);
}
/*if(btnLogin){
    btnLogin.addEventListener('click', loginEmailPassword);
}*/
